# [deeplearning.ai](https://www.deeplearning.ai/)
  
My solution Notebooks of programming assignments of deeplearning.ai's specialization courses on [coursera](https://www.coursera.org/specializations/deep-learning) as of August-2019
  
<br>  

### [Neural Networks and Deep Learning](https://www.coursera.org/learn/neural-networks-deep-learning/home/welcome):   
  
Week 2:
- Logistic Regression as a Neural Network
- Python Basics with Numpy

Week 3:
- Planar data classification with one hidden layer  

Week 4:
- Building your Deep Neural Network - Step by Step
- Deep Neural Network Application Image Classification  
  
  
<br>  

### [Improving Deep Neural Networks](https://www.coursera.org/learn/deep-neural-network/home/welcome):   
  
Week 1:
- Initialization
- Regularization
- Gradient Checking

Week 2:
- Optimization methods  

Week 3:
- Tensorflow Tutorial
  
  
<br>

Owner:  
Subangkar Karmaker Shanto  